import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserComponent } from './component/user/user.component';
import { UserListComponent } from './component/user-list/user-list.component';
import { BlocComponent } from './component/bloc/bloc.component';
import { BlocListComponent } from './component/bloc-list/bloc-list.component';
import {ZeniwoodblockService} from './service/zeniwoodblock.service';
import {HttpClientModule} from '@angular/common/http';
import { AccueilComponent } from './component/accueil/accueil.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { BlocGeneratorComponent } from './component/bloc-generator/bloc-generator.component';
import { BarrelComponent } from './component/barrel/barrel.component';
import { BarrelListComponent } from './component/barrel-list/barrel-list.component';
import { BarrelGeneratorComponent } from './component/barrel-generator/barrel-generator.component';

const appRoutes: Routes = [
  { path: 'user', component: UserComponent },
  { path: 'blocGenerator', component: BlocGeneratorComponent },
  { path: 'blocList', component: BlocListComponent },
  { path: '', component: AccueilComponent },
  { path: 'barrelList', component: BarrelListComponent },
  { path: 'barrelGenerator', component: BarrelGeneratorComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UserListComponent,
    BlocComponent,
    BlocListComponent,
    AccueilComponent,
    BlocGeneratorComponent,
    BarrelComponent,
    BarrelListComponent,
    BarrelGeneratorComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    HttpClientModule,
    RouterModule,
    FormsModule
  ],
  providers: [
    ZeniwoodblockService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
