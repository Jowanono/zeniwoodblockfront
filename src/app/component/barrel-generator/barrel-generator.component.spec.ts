import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarrelGeneratorComponent } from './barrel-generator.component';

describe('BarrelGeneratorComponent', () => {
  let component: BarrelGeneratorComponent;
  let fixture: ComponentFixture<BarrelGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarrelGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarrelGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
