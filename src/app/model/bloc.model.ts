export class BlocModel {

  name: string;
  cost: string;
  brutPrice: string;


  constructor(name: string, cost: string, brutPrice: string) {
    this.name = name;
    this.cost = cost;
    this.brutPrice = brutPrice;
  }
}
