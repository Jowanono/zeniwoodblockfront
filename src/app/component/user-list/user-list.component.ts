import {Component, Input, OnInit} from '@angular/core';
import {ZeniwoodblockService} from '../../service/zeniwoodblock.service';
import {UserModel} from '../../model/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  @Input() userList: UserModel [] = [];

  constructor(private service: ZeniwoodblockService) {
  }

  ngOnInit() {
    this.service.getAllUsers()
      .subscribe(uList => {
        console.log(uList);
        this.userList = uList;
      });
  }
}
