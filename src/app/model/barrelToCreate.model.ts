export class BarrelToCreateModel {

  maxBarrelPrice: number;
  barrelFinish: string;


  constructor(maxBarrelPrice: number, barrelFinish: string) {
    this.maxBarrelPrice = maxBarrelPrice;
    this.barrelFinish = barrelFinish;
  }
}
