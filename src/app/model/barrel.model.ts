export class BarrelModel {

  blocsQuantity: number;
  brutPrice: number;
  netPrice: number;


  constructor(blocsQuantity: number, brutPrice: number, netPrice: number) {
    this.blocsQuantity = blocsQuantity;
    this.brutPrice = brutPrice;
    this.netPrice = netPrice;
  }
}
