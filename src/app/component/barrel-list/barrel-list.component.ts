import {Component, Input, OnInit} from '@angular/core';
import {BlocModel} from '../../model/bloc.model';
import {BarrelModel} from '../../model/barrel.model';
import {ZeniwoodblockService} from '../../service/zeniwoodblock.service';

@Component({
  selector: 'app-barrel-list',
  templateUrl: './barrel-list.component.html',
  styleUrls: ['./barrel-list.component.css']
})
export class BarrelListComponent implements OnInit {

  @Input() barrelList: BarrelModel[] = [];

  constructor(private service: ZeniwoodblockService) {
  }

  ngOnInit() {
    this.service.getAllBarrels()
      .subscribe(bList => {
        console.log(bList);
        this.barrelList = bList;
      });
  }

}
