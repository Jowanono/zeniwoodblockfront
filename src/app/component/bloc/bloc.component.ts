import { Component, OnInit } from '@angular/core';
import {ZeniwoodblockService} from '../../service/zeniwoodblock.service';

@Component({
  selector: 'app-bloc',
  templateUrl: './bloc.component.html',
  styleUrls: ['./bloc.component.css']
})
export class BlocComponent implements OnInit {

  constructor( private service: ZeniwoodblockService) { }

  ngOnInit() {
  }

}
