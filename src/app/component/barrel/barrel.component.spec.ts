import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarrelComponent } from './barrel.component';

describe('BarrelComponent', () => {
  let component: BarrelComponent;
  let fixture: ComponentFixture<BarrelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarrelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarrelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
