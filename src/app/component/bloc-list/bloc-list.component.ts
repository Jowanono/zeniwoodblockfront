import {Component, Input, OnInit} from '@angular/core';
import {BlocModel} from '../../model/bloc.model';
import {ZeniwoodblockService} from '../../service/zeniwoodblock.service';

@Component({
  selector: 'app-bloc-list',
  templateUrl: './bloc-list.component.html',
  styleUrls: ['./bloc-list.component.css']
})
export class BlocListComponent implements OnInit {
  @Input() blocList: BlocModel[] = [];

  constructor(private service: ZeniwoodblockService) { }

  ngOnInit() {
    this.service.getAllBlocs()
      .subscribe(bList => {
        console.log(bList);
        this.blocList = bList;
      });
  }

}
