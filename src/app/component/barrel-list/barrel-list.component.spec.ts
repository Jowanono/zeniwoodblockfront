import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarrelListComponent } from './barrel-list.component';

describe('BarrelListComponent', () => {
  let component: BarrelListComponent;
  let fixture: ComponentFixture<BarrelListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarrelListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarrelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
