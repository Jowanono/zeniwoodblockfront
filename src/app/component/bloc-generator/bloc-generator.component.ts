import {Component, OnInit} from '@angular/core';
import {ZeniwoodblockService} from '../../service/zeniwoodblock.service';
import {BlocToCreateModel} from '../../model/blocToCreate.model';

@Component({
  selector: 'app-bloc-generator',
  templateUrl: './bloc-generator.component.html',
  styleUrls: ['./bloc-generator.component.css']
})
export class BlocGeneratorComponent implements OnInit {

  creationValidation: string;

  constructor(private service: ZeniwoodblockService) {
  }

  ngOnInit() {
  }

  generateBloc() {
    const blocToCreate = new BlocToCreateModel();
    this.service.createBloc(blocToCreate).subscribe(b => {
      console.log(b);
      this.creationValidation = `Votre bloc a bien été généré! Retrouvez le dans l'onglet 'Tous les blocs'`;
    });
  }

}
