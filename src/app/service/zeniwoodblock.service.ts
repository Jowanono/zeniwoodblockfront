import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BlocModel} from '../model/bloc.model';
import {UserModel} from '../model/user.model';
import {BarrelModel} from '../model/barrel.model';
import {BarrelToCreateModel} from '../model/barrelToCreate.model';
import {BlocToCreateModel} from '../model/blocToCreate.model';

@Injectable()
export class ZeniwoodblockService {
  constructor(private http: HttpClient) {
  }

  getAllUsers(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>('http://localhost:8080/ZeniWoodBlock/users');
  }

  getAllBlocs(): Observable<BlocModel []> {
    return this.http.get<BlocModel []>('http://localhost:8080/ZeniWoodBlock/blocs');
  }

  getAllBarrels(): Observable<BarrelModel []> {
    return this.http.get<BarrelModel[]>('http://localhost:8080/ZeniWoodBlock/barrels');
  }

  createBarrel(barrelToCreate: BarrelToCreateModel) {
    return this.http.post('http://localhost:8080/ZeniWoodBlock/randomBarrels', barrelToCreate);
  }

  createBloc(blocToCreate: BlocToCreateModel) {
    return this.http.post('http://localhost:8080/ZeniWoodBlock/randomBlocs', blocToCreate);
  }
}
