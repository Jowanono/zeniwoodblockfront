import { Component, OnInit } from '@angular/core';
import {ZeniwoodblockService} from '../../service/zeniwoodblock.service';
import {BarrelToCreateModel} from '../../model/barrelToCreate.model';

@Component({
  selector: 'app-barrel-generator',
  templateUrl: './barrel-generator.component.html',
  styleUrls: ['./barrel-generator.component.css']
})
export class BarrelGeneratorComponent implements OnInit {
  maxBarrelPrice: number;
  barrelFinish: string;
  creationValidation: string;

  constructor(private service: ZeniwoodblockService) { }

  ngOnInit() {
  }

  generateBarrel() {
    const barrel = new BarrelToCreateModel(this.maxBarrelPrice, this.barrelFinish);
    this.service.createBarrel(barrel).subscribe(b => {
      console.log(b);
      this.creationValidation = `Votre baril a bien été généré! Retrouvez le dans l'onglet 'Tous les barils'`;
    });
  }

}
