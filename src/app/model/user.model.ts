export class UserModel {

  id: string;
  name: string;
  surname: string;
  mailAdress: string;
  adress: string;
  phoneNumber: string;
  password: string;


  constructor(id: string, name: string, surname: string,
              mailAdress: string, adress: string,
              phoneNumber: string, password: string) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.mailAdress = mailAdress;
    this.adress = adress;
    this.phoneNumber = phoneNumber;
    this.password = password;
  }
}
