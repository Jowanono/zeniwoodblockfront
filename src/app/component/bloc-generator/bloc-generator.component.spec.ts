import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlocGeneratorComponent } from './bloc-generator.component';

describe('BlocGeneratorComponent', () => {
  let component: BlocGeneratorComponent;
  let fixture: ComponentFixture<BlocGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlocGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlocGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
